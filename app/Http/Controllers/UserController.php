<?php namespace TMSApp\Http\Controllers;

use TMSApp\Http\Requests;
use TMSApp\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Input;
use JWTAuth;
use TMSApp\User;
use Response;
use HttpResponse;
use Illuminate\Http\Exception\HttpResponseException;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * API function for user login
	 * 
	 * @return Response
	 */
	public function login()
	{
	    $data = Input::all();
	    
	    // Create validator
	    $validator = Validator::make($data, [
	        'email' => 'required|email|max:255',
	        'password' => 'required|min:6',
	    ]);
	    
	    // Thorw message error if fail.
	    if ($validator->fails()) {
	        return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
	    }
	    
	    if (! $token = JWTAuth::attempt($data, ['test' => '123'])) {
	        return Response::json(false, HttpResponse::HTTP_UNAUTHORIZED);
	    }
	    
	    $user = JWTAuth::toUser($token);
	    $payload = JWTAuth::getPayload($token);
	    
	    return Response::json(compact('token', 'user', 'payload'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		
		// Create validator
		$validator = Validator::make($data, [
		    'name' => 'required|max:255',
		    'email' => 'required|email|max:255|unique:users',
		    'password' => 'required|confirmed|min:6',
		]);
		
		// Throw messages error if fail.
		if ($validator->fails()) {
		    return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
		}
		
		// Create new user
		try {
		    $data['password'] = bcrypt($data['password']);
		    $user = User::create($data);
		} catch (\Exception $e) {
		    return Response::json(['error' => trans('message.user_exists')], HttpResponse::HTTP_CONFLICT);
		}
		
		// Generate new jwt token.
		$token = JWTAuth::fromUser($user, ['random_id' => 'test']);
		
		return Response::json(compact('token'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->middleware('jwt-auth');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
