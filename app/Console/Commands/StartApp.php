<?php namespace TMSApp\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Bican\Roles\Models\Role;

class StartApp extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'tms:start';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Start app command for insert sample data into database.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$roles = array(
		    [
		        'name' => 'Admin',
		        'slug' => 'admin',
		        'description' => 'Administrator'
		    ],
		    [
		        'name' => 'Member',
		        'slug' => 'member',
		        'description' => 'Nadia Member'
		    ]
		);
		
		foreach ($roles as $role) {
		    $record = Role::where('name', $role['name'])->first();
		    
		    if (! $record) {
		        $record = Role::create($role);
		    }
		}
		
		$this->info('Insert roles into database completed!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
